const express = require("express");
const serverless = require('serverless-http');
const app = express();

app.get("/timer/start", (req, res) => {
  res.send({ action: "Timer started" });
});

app.get("/timer/pause", (req, res) => {
  res.send({ action: "Timer paused" });
});

app.get("/timer/reset", (req, res) => {
  res.send({ action: "Timer reseted to " + req.query.time });
});

app.get("/score", (req, res) => {
  res.send({
    team1: Math.floor(Math.random() * 10) + 1,
    team2: Math.floor(Math.random() * 10) + 1,
  });
});

app.get("/ball/out", (req, res) => {
  res.send({ action: "ball" });
});

app.get("/led/:code", (req, res) => {
  res.send({ action: "prog: " + req.params.code });
});

app.get("/sound/on", (req, res) => {
  res.send({ action: "sound on" });
});

app.get("/sound/off", (req, res) => {
  res.send({ action: "sound off" });
});

app.get("/sound/plus", (req, res) => {
  res.send({ action: "sound louder" });
});

app.get("/sound/minus", (req, res) => {
  res.send({ action: "sound leiser" });
});

app.get("/sound/normal", (req, res) => {
  res.send({ action: "sound normal" });
});

app.get("/", (req, res) => {
  res.send("Kicker is running");
});



module.exports = app;
module.exports.handler = serverless(app);